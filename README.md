﻿# WebApiSkeleton.FileSystem

`WebApiSkeleton.FileSystem` contains NuGet packages that provide abstractions over a file storage. Default
implementation is done around Minio S3 storage.

## Description

* [`WebApiSkeleton.FileSystem.Abstractions`](./src/WebApiSkeleton.FileSystem.Abstractions) contains abstractions on
  files and directories that can be used around any file system. Also contains classes that are representing different
  file states (downloading, uploading, etc.);
* [`WebApiSkeleton.FileSystem.Contracts`](./src/WebApiSkeleton.FileSystem.Contracts) contains MediatR requests for file
  system implementation;
* [`WebApiSkeleton.FileSystem.MinIO`](./src/WebApiSkeleton.FileSystem.MinIO) is an implementation of file system based
  on Minio S3 storage.

## Usage

### Register the dependencies

Required Minio implementation dependencies are added to DI Container by using `AddMinioAssemblyDependencies` method. All
configuration is done by
modifying [`MinioFileSystemConfiguration`](./src/WebApiSkeleton.FileSystem.MinIO/MinioFileSystemConfiguration.cs)
object.

Following settings are required and will throw if not specified or configured with errors:

* `Endpoint` - endpoint where Minio is located;
* `AccessKey` - minio access key;
* `SecretKey` - minio secret key.

Settings with default values that can be overriden if needed:

* `MaximumInMemoryDownloadFileSizeByte` - maximum file size that is downloaded by using `MemoryStream` (which means file
  is downloaded into server RAM). By default, only files that are sized less than 1 MB are downloaded this way;
* `DefaultBucketName` - the name of default Minio bucket, where files are stored if no directory specified. Default
  is `master`;
* `DefaultDirectorySeparator` - directory separator for file system abstraction. Default is `/`.

### MediatR configuration

Minio implementation uses `WebApiSkeleton` features for MediatR requests, including pipeline behavior validation. Thus,
requests from `WebApiSkeleton.FileSystem.Contracts` implement `IValidatableRequest` and `IAuthorizedRequest`.

Minio implementation module contains validators for all of the commands using `FluentValidation` that are automatically
added to DI container. Although, `ValidationBehavior` from `WebApiSkeleton.Contracts.Validation` is not automatically
added
and must be added manually using methods found
in [documentation](https://gitlab.com/webapiskeleton/nuget/utilities/-/blob/master/src/WebApiSkeleton.Contracts.Validation/README.md?ref_type=heads).
Same is applied to `PermissionBehavior`
from [`WebApiSkeleton.Security`](https://gitlab.com/webapiskeleton/nuget/security).

NOTE: It is highly recommended to add `ValidationBehavior` after `PermissionBehavior` because of performance and
security
reasons.

## Example

Example usage can be found in [`examples`](./examples) directory.

## Versioning

All projects are versioned using following format: `major.minor.patch`. Versioning rules for all projects:

- `patch` needs to be incremented when any minor change is made to the project, such as bugfixes or small
  project-specific features added
- `minor` needs to be incremented when new template-wide feature is implemented. In this case all of the projects must
  have the same version set
- `major` needs to be incremented when the `WebApiSkeleton` template has experienced significant changes, that need to
  upgrade all of the template packages. In this case all of the projects must
  have the same version set
