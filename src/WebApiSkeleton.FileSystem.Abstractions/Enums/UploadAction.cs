﻿namespace WebApiSkeleton.FileSystem.Abstractions.Enums;

public enum UploadAction
{
    /// <summary>
    /// Replaces the file if it already exists
    /// </summary>
    Replace,
    
    /// <summary>
    /// File is only uploaded if it does not exist
    /// </summary>
    AddNew
}