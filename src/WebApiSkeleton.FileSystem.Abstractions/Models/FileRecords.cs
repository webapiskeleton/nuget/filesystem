﻿using System.Text.Json.Serialization;

namespace WebApiSkeleton.FileSystem.Abstractions.Models;

public record FileInformation(string FileName, DirectoryRepresentation? Directory = null)
{
    private string? _fullName;

    /// <summary>
    /// Full name, including directory information separated by <see cref="FileSystemSettings.DirectorySeparator"/>
    /// </summary>
    public string FullName => _fullName ??=
        $"{(Directory is null ? "" : $"{Directory.FullName}{FileSystemSettings.DirectorySeparator}")}{FileName}";
}

public record DetailedFileInformation(
    FileInformation File,
    string ContentType,
    long Size);

public record FileStats(
    DetailedFileInformation File,
    DateTime LastModified,
    Dictionary<string, string> AdditionalMetadata);

public record DownloadingFile(DetailedFileInformation FileInfo, Stream DownloadStream)
{
    /// <summary>
    /// Stream that contains a downloaded file
    /// </summary>
    public Stream DownloadStream { get; } = DownloadStream;
}

public record UploadingFile(FileInformation FileInfo, string ContentType, Stream UploadStream)
{
    /// <summary>
    /// Stream that is used to read file data and upload it to provider
    /// </summary>
    [JsonIgnore]
    public Stream UploadStream { get; } = UploadStream;
}