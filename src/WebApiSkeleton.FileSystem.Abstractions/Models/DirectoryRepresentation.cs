﻿namespace WebApiSkeleton.FileSystem.Abstractions.Models;

public record DirectoryRepresentation
{
    private string? _fullName;

    public string Name { get; }
    
    /// <summary>
    /// Directory full name separated by <see cref="FileSystemSettings.DirectorySeparator"/>
    /// </summary>
    public string FullName => _fullName ?? GetFullName();
    public DirectoryRepresentation? Parent { get; }


    public DirectoryRepresentation(string name, DirectoryRepresentation? parent = null)
    {
        Name = name;
        Parent = parent;
    }

    private string GetFullName()
    {
        var directoryNames = new List<string> { Name };
        var parent = Parent;
        while (parent is not null)
        {
            directoryNames.Add(parent.Name);
            parent = parent.Parent;
        }

        var res = string.Join(FileSystemSettings.DirectorySeparator, directoryNames.AsEnumerable().Reverse());
        _fullName = res;
        return res;
    }
}