﻿namespace WebApiSkeleton.FileSystem.Abstractions.Models;

/// <summary>
/// File stream that deletes a file on being disposed
/// </summary>
public class DeleteOnDisposeFileStream : FileStream
{
    private readonly string _path;

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (!disposing) return;
        if (Path.Exists(_path))
        {
            File.Delete(_path);
        }
    }

    public DeleteOnDisposeFileStream(string path, FileMode mode) : base(path, mode)
    {
        _path = path;
    }
}