﻿using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.Abstractions.Interfaces;

public interface IFileAccessor
{
    /// <summary>
    /// Get detailed file stats, including its <see cref="FileStats.AdditionalMetadata"/>
    /// </summary>
    /// <param name="file">File to get information about</param>
    /// <param name="cancellationToken"></param>
    /// <returns>File stats if found, otherwise null</returns>
    public Task<FileStats?> GetFileStatistics(FileInformation file, CancellationToken cancellationToken = default);

    /// <summary>
    /// Rename the provided file and leave it in original directory
    /// </summary>
    /// <param name="file">File to rename</param>
    /// <param name="newName">New file name</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Updated information about the file</returns>
    public Task<FileInformation> RenameFile(FileInformation file, string newName,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Move file to different directory without changing its name
    /// </summary>
    /// <param name="file">File to move</param>
    /// <param name="newLocation">New directory to move file to</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Updated information about the file</returns>
    public Task<FileInformation> MoveFile(FileInformation file, DirectoryRepresentation? newLocation,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Delete file from file system
    /// </summary>
    /// <param name="file">File to delete</param>
    /// <param name="cancellationToken"></param>
    public Task DeleteFile(FileInformation file, CancellationToken cancellationToken = default);
}