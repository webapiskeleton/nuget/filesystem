﻿using WebApiSkeleton.FileSystem.Abstractions.Enums;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.Abstractions.Interfaces;

public interface IFileUploader
{
    /// <summary>
    /// Upload a file to the system
    /// </summary>
    /// <param name="file">File to upload, including the <see cref="UploadingFile.UploadStream"/></param>
    /// <param name="action">Defines that file should be rewritten if exists when set to <see cref="UploadAction.Replace"/> or cancel cancel upload if set to <see cref="UploadAction.AddNew"/></param>
    /// <param name="cancellationToken"></param>
    /// <returns>Uploaded file information</returns>
    public Task<DetailedFileInformation> UploadFile(UploadingFile file, UploadAction action = UploadAction.AddNew,
        CancellationToken cancellationToken = default);
}