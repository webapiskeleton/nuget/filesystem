﻿using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.Abstractions.Interfaces;

public interface IFileDownloader
{
    /// <summary>
    /// Download file and get its <see cref="DownloadingFile.DownloadStream"/>
    /// </summary>
    /// <param name="file">Downloading file information</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Information about file downloading and a <see cref="DownloadingFile.DownloadStream"/> that contains file contents</returns>
    public Task<DownloadingFile> DownloadFile(FileInformation file, CancellationToken cancellationToken = default);

    /// <summary>
    /// Download a file to given stream
    /// </summary>
    /// <param name="file">Downloading file information</param>
    /// <param name="streamToDownloadTo">Stream to upload file to</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Information about file downloading. <see cref="DownloadingFile.DownloadStream"/> contains the stream passed to method arguments</returns>
    public Task<DownloadingFile> DownloadFile(FileInformation file, Stream streamToDownloadTo,
        CancellationToken cancellationToken = default);

    /// <summary>
    /// Downloads multiple files in a ZIP archive
    /// </summary>
    /// <param name="files">Files to download</param>
    /// <param name="cancellationToken"></param>
    /// <returns>File information about ZIP archive created</returns>
    /// <remarks>ZIP archive is being leftover in OS temp files, it is better to clean it somehow</remarks>
    public Task<DetailedFileInformation> DownloadFiles(IEnumerable<FileInformation> files,
        CancellationToken cancellationToken = default);
}