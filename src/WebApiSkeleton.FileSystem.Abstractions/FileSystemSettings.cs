﻿namespace WebApiSkeleton.FileSystem.Abstractions;

public static class FileSystemSettings
{
    public static string DirectorySeparator { get; set; } = "/";
}