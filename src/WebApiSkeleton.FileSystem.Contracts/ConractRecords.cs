﻿using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.FileSystem.Abstractions.Enums;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.Contracts;

public interface IFileSystemRequest
{
}

/// <summary>
/// Download file from file system. If <see cref="StreamToDownloadTo"/> is not null, file contents are downloaded there
/// </summary>
/// <param name="File">File to download</param>
/// <param name="StreamToDownloadTo">Stream to copy file contents to</param>
public sealed record DownloadFileCommand(FileInformation File, Stream? StreamToDownloadTo = null)
    : IAuthorizedRequest<DownloadingFile>, IValidatableRequest<DownloadingFile>, IFileSystemRequest;

/// <summary>
/// Download many files from file system. They are all placed in one archive returned
/// </summary>
/// <param name="Files">Files to download</param>
public sealed record DownloadFilesArchiveCommand(IEnumerable<FileInformation> Files)
    : IAuthorizedRequest<DetailedFileInformation>, IValidatableRequest<DetailedFileInformation>, IFileSystemRequest;

/// <summary>
/// Upload file to file system
/// </summary>
/// <param name="File">Uploading file information</param>
/// <param name="Action">Action to use when file already exists</param>
public sealed record UploadFileCommand(UploadingFile File, UploadAction Action = UploadAction.AddNew)
    : IAuthorizedRequest<DetailedFileInformation>, IValidatableRequest<DetailedFileInformation>, IFileSystemRequest;

/// <summary>
/// Rename the provided file and leave it in original directory
/// </summary>
/// <param name="File">File to rename</param>
/// <param name="NewName">New file name</param>
public sealed record RenameFileCommand(FileInformation File, string NewName) : IAuthorizedRequest<FileInformation>,
    IValidatableRequest<FileInformation>, IFileSystemRequest;

/// <summary>
/// Move file to different directory without changing its name
/// </summary>
/// <param name="File">File to move</param>
/// <param name="NewLocation">New directory to move file to</param>
public sealed record MoveFileCommand(FileInformation File, DirectoryRepresentation? NewLocation)
    : IAuthorizedRequest<FileInformation>, IValidatableRequest<FileInformation>, IFileSystemRequest;

/// <summary>
/// Delete file from file system
/// </summary>
/// <param name="File">File to delete</param>
public sealed record DeleteFileCommand(FileInformation File) : IAuthorizedRequest<FileInformation>,
    IValidatableRequest<FileInformation>, IFileSystemRequest;

/// <summary>
/// Get detailed file stats, including its <see cref="FileStats.AdditionalMetadata"/>
/// </summary>
/// <param name="File">File to get information about</param>
public sealed record GetFileStatisticsCommand(FileInformation File)
    : IAuthorizedRequest<FileStats?>, IFileSystemRequest;