﻿namespace WebApiSkeleton.FileSystem.MinIO;

/// <summary>
/// File representation to work with in minio environment 
/// </summary>
/// <param name="BucketName">Bucket name</param>
/// <param name="FileName">Object name in bucket</param>
internal record MinioFileRepresentation(string BucketName, string FileName);