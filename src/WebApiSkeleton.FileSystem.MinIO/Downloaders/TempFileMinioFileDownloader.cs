﻿using Minio;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.MinIO.Downloaders;

/// <summary>
/// Downloader that loads the whole file into temp file that is deleted on filestream dispose
/// </summary>
public sealed class TempFileMinioFileDownloader : BaseMinioFileDownloader
{
    public TempFileMinioFileDownloader(MinioSettings settings, IMinioClient client) : base(settings, client)
    {
    }

    public override async Task<DownloadingFile> DownloadFile(FileInformation file,
        CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file, Settings.DefaultBucketName);
        var toReturn = new DeleteOnDisposeFileStream(Path.GetTempFileName(), FileMode.Create);
        var args = new GetObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName)
            .WithCallbackStream(stream =>
            {
                stream.CopyTo(toReturn);
                toReturn.Seek(0, SeekOrigin.Begin);
            });
        var res = await Client.GetObjectAsync(args, cancellationToken);
        return new DownloadingFile(new DetailedFileInformation(file, res.ContentType, res.Size), toReturn);
    }
}