﻿using Minio;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.MinIO.Downloaders;

/// <summary>
/// Downloader that loads the whole file into MemoryStream and returning it
/// </summary>
public sealed class InMemoryMinioFileDownloader : BaseMinioFileDownloader
{
    public InMemoryMinioFileDownloader(MinioSettings settings, IMinioClient client) : base(settings, client)
    {
    }

    public override async Task<DownloadingFile> DownloadFile(FileInformation file,
        CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file, Settings.DefaultBucketName);
        var toReturn = new MemoryStream();
        var args = new GetObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName)
            .WithCallbackStream(stream =>
            {
                stream.CopyTo(toReturn);
                toReturn.Seek(0, SeekOrigin.Begin);
            });
        var res = await Client.GetObjectAsync(args, cancellationToken);
        return new DownloadingFile(new DetailedFileInformation(file, res.ContentType, res.Size), toReturn);
    }
}