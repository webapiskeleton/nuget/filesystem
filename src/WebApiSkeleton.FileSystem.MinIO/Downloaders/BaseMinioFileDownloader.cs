﻿using System.IO.Compression;
using Minio;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using DownloadedObject = (string FileName, string ObjectName);

namespace WebApiSkeleton.FileSystem.MinIO.Downloaders;

public abstract class BaseMinioFileDownloader : IFileDownloader
{
    protected readonly MinioSettings Settings;
    protected readonly IMinioClient Client;

    protected BaseMinioFileDownloader(MinioSettings settings, IMinioClient client)
    {
        Settings = settings;
        Client = client;
    }

    /// <inheritdoc/>
    public abstract Task<DownloadingFile> DownloadFile(FileInformation file,
        CancellationToken cancellationToken = default);

    /// <inheritdoc/>
    public async Task<DownloadingFile> DownloadFile(FileInformation file, Stream streamToDownloadTo,
        CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file, Settings.DefaultBucketName);
        var args = new GetObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName)
            .WithCallbackStream(async (s, token) => { await s.CopyToAsync(streamToDownloadTo, token); });
        var res = await Client.GetObjectAsync(args, cancellationToken);
        return new DownloadingFile(new DetailedFileInformation(file, res.ContentType, res.Size), streamToDownloadTo);
    }

    /// <inheritdoc/>
    /// <remarks>Files that are downloaded while creating an archive are being automatically deleted by default. To prevent this behavior override <see cref="AfterFileArchived"/> method</remarks>
    public async Task<DetailedFileInformation> DownloadFiles(IEnumerable<FileInformation> files,
        CancellationToken cancellationToken = default)
    {
        var zipName = Path.GetTempFileName();
        await using var archiveStream = new FileStream(zipName, FileMode.Create);
        using var archive = new ZipArchive(archiveStream, ZipArchiveMode.Create);
        foreach (var file in files)
        {
            var downloadedFile = await GetObjectDownloadedToFile(file, cancellationToken);
            archive.CreateEntryFromFile(downloadedFile.FileName, downloadedFile.ObjectName);
            await AfterFileArchived(downloadedFile);
        }

        return new DetailedFileInformation(new FileInformation(zipName), "application/zip", archiveStream.Length);
    }

    protected virtual async Task<DownloadedObject> GetObjectDownloadedToFile(FileInformation file,
        CancellationToken cancellationToken = default)
    {
        var fileName = Path.GetTempFileName();
        var fileInfo = MinioFileParser.ParseFileInformation(file, Settings.DefaultBucketName);
        var args = new GetObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName)
            .WithFile(fileName);
        var res = await Client.GetObjectAsync(args, cancellationToken);

        return (fileName, res.ObjectName);
    }

    protected virtual Task AfterFileArchived(DownloadedObject downloadedObject)
    {
        if (File.Exists(downloadedObject.FileName))
            File.Delete(downloadedObject.FileName);

        return Task.CompletedTask;
    }
}