﻿using System.Collections.Concurrent;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.MinIO.Cache;

internal interface IRequestScopeCache
{
    /// <summary>
    /// Get detailed file stats, including its <see cref="FileStats.AdditionalMetadata"/>
    /// </summary>
    /// <param name="file">File to get information about</param>
    /// <param name="cancellationToken"></param>
    /// <returns>File stats if found, otherwise null</returns>
    /// <remarks>Difference between this method and <see cref="IFileAccessor.GetFileStatistics"/> is that implementation of this method should also cache the file some way not to make too many requests in scope of one MediatR request</remarks>
    public Task<FileStats?> GetFileStatistics(FileInformation file, CancellationToken cancellationToken = default);
}

internal sealed class RequestScopeCache : IRequestScopeCache
{
    private readonly IFileAccessor _fileAccessor;
    private readonly ConcurrentDictionary<FileInformation, FileStats?> _cacheDictionary = new();

    public RequestScopeCache(IFileAccessor fileAccessor)
    {
        _fileAccessor = fileAccessor;
    }
    
    /// <inheritdoc/>
    /// <remarks><inheritdoc/>. Caches the file information and its stats in concurrent dictionary</remarks>
    public async Task<FileStats?> GetFileStatistics(FileInformation file, CancellationToken cancellationToken = default)
    {
        if (_cacheDictionary.TryGetValue(file, out var value))
            return value;

        var stats = await _fileAccessor.GetFileStatistics(file, cancellationToken);
        _cacheDictionary.TryAdd(file, stats);
        return stats;
    }
}