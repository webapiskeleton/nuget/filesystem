﻿using Minio;
using WebApiSkeleton.FileSystem.Abstractions.Enums;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.MinIO.Uploaders;

public sealed class MinioFileUploader : IFileUploader
{
    private readonly MinioSettings _settings;
    private readonly IMinioClient _client;

    public MinioFileUploader(MinioSettings settings, IMinioClient client)
    {
        _settings = settings;
        _client = client;
    }

    ///<inheritdoc/>
    public async Task<DetailedFileInformation> UploadFile(UploadingFile file, UploadAction action = UploadAction.AddNew,
        CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file.FileInfo, _settings.DefaultBucketName);
        var putObjectArgs = new PutObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName)
            .WithStreamData(file.UploadStream)
            .WithContentType(file.ContentType)
            .WithObjectSize(file.UploadStream.Length);
        var res = await _client.PutObjectAsync(putObjectArgs, cancellationToken);
        return new DetailedFileInformation(file.FileInfo, file.ContentType, res.Size);
    }
}