﻿using LanguageExt.Common;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class DownloadFilesArchiveCommandHandler :
    IRequestHandler<DownloadFilesArchiveCommand, Result<DetailedFileInformation>>
{
    private readonly IServiceProvider _serviceProvider;

    public DownloadFilesArchiveCommandHandler(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task<Result<DetailedFileInformation>> Handle(DownloadFilesArchiveCommand request,
        CancellationToken cancellationToken)
    {
        var fileDownloader = _serviceProvider.GetRequiredKeyedService<IFileDownloader>("tempfile");
        var archiveInfo = await fileDownloader.DownloadFiles(request.Files, cancellationToken);
        return archiveInfo;
    }
}