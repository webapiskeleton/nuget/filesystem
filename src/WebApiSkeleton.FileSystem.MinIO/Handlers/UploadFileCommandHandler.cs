﻿using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.FileSystem.Abstractions.Enums;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class UploadFileCommandHandler : IRequestHandler<UploadFileCommand, Result<DetailedFileInformation>>
{
    private readonly IFileUploader _fileUploader;
    private readonly IFileAccessor _fileAccessor;

    public UploadFileCommandHandler(IFileUploader fileUploader, IFileAccessor fileAccessor)
    {
        _fileUploader = fileUploader;
        _fileAccessor = fileAccessor;
    }

    public async Task<Result<DetailedFileInformation>> Handle(UploadFileCommand request,
        CancellationToken cancellationToken)
    {
        if (request.Action == UploadAction.Replace)
        {
            return await _fileUploader.UploadFile(request.File, request.Action, cancellationToken);
        }

        if (await _fileAccessor.GetFileStatistics(request.File.FileInfo, cancellationToken) is not null)
        {
            return new Result<DetailedFileInformation>(new InvalidOperationException("File already exists"));
        }

        return await _fileUploader.UploadFile(request.File, request.Action, cancellationToken);
    }
}