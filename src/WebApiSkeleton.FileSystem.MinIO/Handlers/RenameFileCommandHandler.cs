﻿using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class RenameFileCommandHandler : IRequestHandler<RenameFileCommand, Result<FileInformation>>
{
    private readonly IFileAccessor _fileAccessor;

    public RenameFileCommandHandler(IFileAccessor fileAccessor)
    {
        _fileAccessor = fileAccessor;
    }

    public async Task<Result<FileInformation>> Handle(RenameFileCommand request, CancellationToken cancellationToken)
    {
        return await _fileAccessor.RenameFile(request.File, request.NewName, cancellationToken);
    }
}