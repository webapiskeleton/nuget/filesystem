﻿using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class MoveFileCommandHandler : IRequestHandler<MoveFileCommand, Result<FileInformation>>
{
    private readonly IFileAccessor _fileAccessor;

    public MoveFileCommandHandler(IFileAccessor fileAccessor)
    {
        _fileAccessor = fileAccessor;
    }

    public async Task<Result<FileInformation>> Handle(MoveFileCommand request, CancellationToken cancellationToken)
    {
        return await _fileAccessor.MoveFile(request.File, request.NewLocation, cancellationToken);
    }
}