﻿using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class DeleteFileCommandHandler : IRequestHandler<DeleteFileCommand, Result<FileInformation>>
{
    private readonly IFileAccessor _fileAccessor;

    public DeleteFileCommandHandler(IFileAccessor fileAccessor)
    {
        _fileAccessor = fileAccessor;
    }

    public async Task<Result<FileInformation>> Handle(DeleteFileCommand request, CancellationToken cancellationToken)
    {
        await _fileAccessor.DeleteFile(request.File, cancellationToken);
        return request.File;
    }
}