﻿using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class GetFileStatisticsCommandHandler : IRequestHandler<GetFileStatisticsCommand, Result<FileStats?>>
{
    private readonly IFileAccessor _accessor;

    public GetFileStatisticsCommandHandler(IFileAccessor accessor)
    {
        _accessor = accessor;
    }

    public async Task<Result<FileStats?>> Handle(GetFileStatisticsCommand request, CancellationToken cancellationToken)
    {
        return await _accessor.GetFileStatistics(request.File, cancellationToken);
    }
}