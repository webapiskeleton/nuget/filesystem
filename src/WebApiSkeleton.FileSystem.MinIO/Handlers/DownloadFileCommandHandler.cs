﻿using LanguageExt.Common;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO.Cache;

namespace WebApiSkeleton.FileSystem.MinIO.Handlers;

internal sealed class DownloadFileCommandHandler : IRequestHandler<DownloadFileCommand, Result<DownloadingFile>>
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IRequestScopeCache _cache;
    private readonly MinioSettings _settings;

    public DownloadFileCommandHandler(IServiceProvider serviceProvider, IRequestScopeCache cache,
        MinioSettings settings)
    {
        _serviceProvider = serviceProvider;
        _cache = cache;
        _settings = settings;
    }

    public async Task<Result<DownloadingFile>> Handle(DownloadFileCommand request, CancellationToken cancellationToken)
    {
        IFileDownloader downloader;
        if (request.StreamToDownloadTo is not null)
        {
            downloader = _serviceProvider.GetRequiredKeyedService<IFileDownloader>("inmemory");
            return await downloader.DownloadFile(request.File, request.StreamToDownloadTo, cancellationToken);
        }

        var fileInfo = await _cache.GetFileStatistics(request.File, cancellationToken);
        if (fileInfo!.File.Size < _settings.MaximumInMemoryDownloadFileSizeByte)
        {
            downloader = _serviceProvider.GetRequiredKeyedService<IFileDownloader>("inmemory");
            return await downloader.DownloadFile(request.File, cancellationToken);
        }

        downloader = _serviceProvider.GetRequiredKeyedService<IFileDownloader>("tempfile");
        return await downloader.DownloadFile(request.File, cancellationToken);
    }
}