﻿using Minio;
using Minio.Exceptions;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.MinIO.Accessors;

public sealed class MinioFileAccessor : IFileAccessor
{
    private readonly IMinioClient _client;
    private readonly MinioSettings _settings;

    public MinioFileAccessor(IMinioClient client, MinioSettings settings)
    {
        _client = client;
        _settings = settings;
    }

    /// <inheritdoc/>
    /// <remarks>Minio throws an <see cref="ObjectNotFoundException"/> or <see cref="BucketNotFoundException"/> when file is not found, so these exceptions are being leftover returning null as file does not exist</remarks>
    public async Task<FileStats?> GetFileStatistics(FileInformation file, CancellationToken cancellationToken = default)
    {
        try
        {
            var fileInfo = MinioFileParser.ParseFileInformation(file, _settings.DefaultBucketName);
            var statArgs = new StatObjectArgs()
                .WithBucket(fileInfo.BucketName)
                .WithObject(fileInfo.FileName);
            var res = await _client.StatObjectAsync(statArgs, cancellationToken);
            return new FileStats(new DetailedFileInformation(file, res.ContentType, res.Size), res.LastModified,
                res.MetaData);
        }
        catch (ObjectNotFoundException)
        {
            return null;
        }
        catch (BucketNotFoundException)
        {
            return null;
        }
    }

    /// <inheritdoc/>
    /// <remarks>Minio does not have a way to rename file, so the file is being copied with new name and the old one is being deleted</remarks>
    public async Task<FileInformation> RenameFile(FileInformation file, string newName,
        CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file, _settings.DefaultBucketName);
        var copyArgs = new CopyObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithCopyObjectSource(new CopySourceObjectArgs().WithBucket(fileInfo.BucketName)
                .WithObject(fileInfo.FileName))
            .WithObject(newName);
        var deleteArgs = new RemoveObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName);
        await _client.CopyObjectAsync(copyArgs, cancellationToken);
        await _client.RemoveObjectAsync(deleteArgs, cancellationToken);
        return new FileInformation(newName, file.Directory);
    }

    /// <inheritdoc/>
    /// <remarks>Minio does not have a way to move file, so the file is being copied to the new place and the old one is being deleted</remarks>
    public async Task<FileInformation> MoveFile(FileInformation file, DirectoryRepresentation? newLocation,
        CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file, _settings.DefaultBucketName);
        var newFileInfo = MinioFileParser.ParseFileInformation(new FileInformation(file.FileName, newLocation),
            _settings.DefaultBucketName);
        var copyArgs = new CopyObjectArgs()
            .WithBucket(newFileInfo.BucketName)
            .WithCopyObjectSource(new CopySourceObjectArgs()
                .WithBucket(fileInfo.BucketName)
                .WithObject(fileInfo.FileName))
            .WithObject(fileInfo.FileName);
        var deleteArgs = new RemoveObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName);
        await _client.CopyObjectAsync(copyArgs, cancellationToken);
        await _client.RemoveObjectAsync(deleteArgs, cancellationToken);

        return new FileInformation(file.FileName, newLocation);
    }

    /// <inheritdoc/>
    public async Task DeleteFile(FileInformation file, CancellationToken cancellationToken = default)
    {
        var fileInfo = MinioFileParser.ParseFileInformation(file, _settings.DefaultBucketName);
        var removeArgs = new RemoveObjectArgs()
            .WithBucket(fileInfo.BucketName)
            .WithObject(fileInfo.FileName);
        await _client.RemoveObjectAsync(removeArgs, cancellationToken);
    }
}