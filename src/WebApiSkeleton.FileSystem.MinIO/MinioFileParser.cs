﻿using WebApiSkeleton.FileSystem.Abstractions;
using WebApiSkeleton.FileSystem.Abstractions.Models;

namespace WebApiSkeleton.FileSystem.MinIO;

internal static class MinioFileParser
{
    /// <summary>
    /// Parse file information abstraction and convert it to <see cref="MinioFileRepresentation"/>
    /// </summary>
    /// <param name="file">File information abstraction</param>
    /// <param name="defaultBucketName">Default bucket name if <see cref="FileInformation.Directory"/> is null</param>
    /// <remarks>Symbols before the first <see cref="FileSystemSettings.DirectorySeparator"/> are perceived as a bucket name</remarks>
    public static MinioFileRepresentation ParseFileInformation(FileInformation file, string defaultBucketName)
    {
        var bucket = file.Directory?.FullName.Split(FileSystemSettings.DirectorySeparator)[0] ?? defaultBucketName;
        var fileName = file.Directory switch
        {
            null => file.FileName,
            _ => string.Join(FileSystemSettings.DirectorySeparator,
                file.FullName.Split(FileSystemSettings.DirectorySeparator).Skip(1))
        };
        return new MinioFileRepresentation(bucket, fileName);
    }
}