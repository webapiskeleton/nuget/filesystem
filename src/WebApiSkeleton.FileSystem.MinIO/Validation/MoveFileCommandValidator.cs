﻿using FluentValidation;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO.Cache;

namespace WebApiSkeleton.FileSystem.MinIO.Validation;

internal sealed class MoveFileCommandValidator : AbstractValidator<MoveFileCommand>
{
    public MoveFileCommandValidator(IRequestScopeCache cache)
    {
        ClassLevelCascadeMode = CascadeMode.Stop;

        RuleFor(x => x.File)
            .MustAsync(async (file, token) =>
            {
                var stats = await cache.GetFileStatistics(file, token);
                return stats is not null;
            })
            .WithMessage("File does not exist");

        RuleFor(x => new FileInformation(x.File.FileName, x.NewLocation))
            .MustAsync(async (file, token) =>
            {
                var stats = await cache.GetFileStatistics(file, token);
                return stats is null;
            })
            .WithMessage("File with this name already exists in this location");
    }
}