﻿using FluentValidation;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO.Cache;

namespace WebApiSkeleton.FileSystem.MinIO.Validation;

internal sealed class RenameFileCommandValidator : AbstractValidator<RenameFileCommand>
{
    public RenameFileCommandValidator(IRequestScopeCache cache)
    {
        ClassLevelCascadeMode = CascadeMode.Stop;
        RuleFor(x => x.NewName)
            .NotEmpty()
            .WithMessage("New name cannot be empty")
            .NotNull()
            .WithMessage("New name cannot be null");
        
        RuleFor(x => x.File)
            .MustAsync(async (file, token) =>
            {
                var stats = await cache.GetFileStatistics(file, token);
                return stats is not null;
            })
            .WithMessage("File does not exist");
        RuleFor(x => new FileInformation(x.NewName, x.File.Directory))
            .MustAsync(async (file, token) =>
            {
                var stats = await cache.GetFileStatistics(file, token);
                return stats is null;
            })
            .WithMessage("File with this name already exists");
    }
}