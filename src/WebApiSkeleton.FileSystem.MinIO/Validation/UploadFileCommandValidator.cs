﻿using FluentValidation;
using WebApiSkeleton.FileSystem.Abstractions.Enums;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO.Cache;

namespace WebApiSkeleton.FileSystem.MinIO.Validation;

internal sealed class UploadFileCommandValidator : AbstractValidator<UploadFileCommand>
{
    public UploadFileCommandValidator(IRequestScopeCache cache)
    {
        RuleFor(x => new { x.File, x.Action })
            .MustAsync(async (info, token) =>
            {
                var file = await cache.GetFileStatistics(info.File.FileInfo, token);
                return file is null;
            })
            .When(x => x.Action == UploadAction.AddNew)
            .WithMessage("File already exists");
    }
}