﻿using System.Collections.Concurrent;
using FluentValidation;
using FluentValidation.Results;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO.Cache;

namespace WebApiSkeleton.FileSystem.MinIO.Validation;

internal sealed class DownloadFilesArchiveCommandValidator : AbstractValidator<DownloadFilesArchiveCommand>
{
    public DownloadFilesArchiveCommandValidator(IRequestScopeCache cache)
    {
        RuleFor(x => x.Files)
            .CustomAsync(async (files, context, token) =>
            {
                var nonExistingFiles = new ConcurrentDictionary<string, FileInformation>();
                await Parallel.ForEachAsync(files,
                    new ParallelOptions { MaxDegreeOfParallelism = 4, CancellationToken = token },
                    async (info, cancellationToken) =>
                    {
                        var res = await cache.GetFileStatistics(info, cancellationToken);
                        if (res is null)
                            nonExistingFiles.TryAdd(info.FullName, info);
                    });
                if (nonExistingFiles.IsEmpty)
                    return;

                foreach (var nonExistingFile in nonExistingFiles)
                {
                    context.AddFailure(new ValidationFailure(nameof(DownloadFilesArchiveCommand.Files),
                        $"File does not exist: {nonExistingFile.Value.FullName}"));
                }
            });
    }
}