﻿using FluentValidation;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO.Cache;

namespace WebApiSkeleton.FileSystem.MinIO.Validation;

internal sealed class DownloadFileCommandValidator : AbstractValidator<DownloadFileCommand>
{
    public DownloadFileCommandValidator(IRequestScopeCache cache)
    {
        RuleFor(x => x.File)
            .MustAsync(async (information, token) =>
            {
                var fileStatistics = await cache.GetFileStatistics(information, token);
                return fileStatistics is not null;
            })
            .WithMessage("File does not exist");
    }
}