﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Minio;
using Minio.AspNetCore;
using Minio.AspNetCore.HealthChecks;
using WebApiSkeleton.Contracts.Validation;
using WebApiSkeleton.FileSystem.Abstractions;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.MinIO.Accessors;
using WebApiSkeleton.FileSystem.MinIO.Cache;
using WebApiSkeleton.FileSystem.MinIO.Downloaders;
using WebApiSkeleton.FileSystem.MinIO.Services;
using WebApiSkeleton.FileSystem.MinIO.Uploaders;

namespace WebApiSkeleton.FileSystem.MinIO;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddMinioAssemblyDependencies(this IServiceCollection services,
        Action<MinioFileSystemConfiguration> configurationAction)
    {
        var config = new MinioFileSystemConfiguration();
        configurationAction.Invoke(config);
        config.ValidateConfiguration();

        FileSystemSettings.DirectorySeparator = config.DefaultDirectorySeparator;

        services.AddMinio(options =>
        {
            options.Endpoint = config.Endpoint!;

            options.ConfigureClient(x => x.WithCredentials(config.AccessKey!, config.SecretKey!));
        });

        services.AddContractValidatorsFromAssembly(typeof(MinioSettings).Assembly, includeInternalTypes: true);

        var settings = new MinioSettings
        {
            DefaultBucketName = config.DefaultBucketName,
            MaximumInMemoryDownloadFileSizeByte = config.MaximumInMemoryDownloadFileSizeByte
        };
        services.TryAddSingleton(settings);

        services.AddMediatR(x => { x.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()); });

        services.TryAddTransient<IRequestScopeCache, RequestScopeCache>();
        services.TryAddKeyedSingleton<IFileDownloader, InMemoryMinioFileDownloader>("inmemory");
        services.TryAddKeyedSingleton<IFileDownloader, TempFileMinioFileDownloader>("tempfile");
        services.TryAddSingleton<IFileUploader, MinioFileUploader>();
        services.TryAddSingleton<IFileAccessor, MinioFileAccessor>();
        services.TryAddSingleton<IMinioService, MinioService>();
        return services;
    }
}