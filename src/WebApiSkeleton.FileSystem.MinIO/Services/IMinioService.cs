﻿namespace WebApiSkeleton.FileSystem.MinIO.Services;

public interface IMinioService
{
    public Task<bool> CreateBucket(string bucketName, CancellationToken cancellationToken = default);
}