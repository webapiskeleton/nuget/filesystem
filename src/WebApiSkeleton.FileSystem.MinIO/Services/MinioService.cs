﻿using Minio;

namespace WebApiSkeleton.FileSystem.MinIO.Services;

public sealed class MinioService : IMinioService
{
    private readonly IMinioClient _client;

    public MinioService(IMinioClient client)
    {
        _client = client;
    }

    public async Task<bool> CreateBucket(string bucketName, CancellationToken cancellationToken = default)
    {
        var existsArgs = new BucketExistsArgs()
            .WithBucket(bucketName);
        var exists = await _client.BucketExistsAsync(existsArgs, cancellationToken);
        if (exists)
            return false;

        var createArgs = new MakeBucketArgs()
            .WithBucket(bucketName);
        await _client.MakeBucketAsync(createArgs, cancellationToken);
        return true;
    }
}