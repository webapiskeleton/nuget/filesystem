﻿namespace WebApiSkeleton.FileSystem.MinIO;

public sealed class MinioSettings
{
    public string DefaultBucketName { get; init; } = "master";

    /// <summary>
    /// Maximum file size to download via memorystream (default is 1MB)
    /// </summary>
    public long MaximumInMemoryDownloadFileSizeByte = 1 * 1024 * 1024;
}