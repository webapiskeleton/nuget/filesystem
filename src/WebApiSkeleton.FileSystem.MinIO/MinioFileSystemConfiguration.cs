﻿using System.Configuration;

namespace WebApiSkeleton.FileSystem.MinIO;

public sealed class MinioFileSystemConfiguration
{
    /// <summary>
    /// Minio endpoint
    /// </summary>
    public string? Endpoint { get; set; }
    public string? AccessKey { get; set; }
    public string? SecretKey { get; set; }

    /// <summary>
    /// Maximum file size that is downloaded using MemoryStream
    /// </summary>
    public long MaximumInMemoryDownloadFileSizeByte = 1 * 1024 * 1024;
    public string DefaultBucketName { get; set; } = "master";
    public string DefaultDirectorySeparator = "/";

    internal void ValidateConfiguration()
    {
        if (string.IsNullOrEmpty(Endpoint))
            throw new ConfigurationErrorsException("No Minio endpoint provided");
        if (string.IsNullOrEmpty(AccessKey))
            throw new ConfigurationErrorsException("No Minio access key provided");
        if (string.IsNullOrEmpty(SecretKey))
            throw new ConfigurationErrorsException("No Minio secret key provided");
    }
}