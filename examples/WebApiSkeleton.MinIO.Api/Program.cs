using MediatR;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using WebApiSkeleton.Contracts.Validation;
using WebApiSkeleton.FileSystem.Abstractions.Interfaces;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO;
using WebApiSkeleton.MinIO.Api;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMinioAssemblyDependencies(config =>
    {
        config.Endpoint = builder.Configuration["Minio:Endpoint"];
        config.AccessKey = builder.Configuration["Minio:AccessKey"];
        config.SecretKey = builder.Configuration["Minio:SecretKey"];
    })
    .AddValidationPipelineBehaviorsFromAssembly(typeof(MinioSettings).Assembly);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapPost("/upload",
        async ([FromServices] ISender sender, [FromForm] IFormFile file) =>
        {
            await using var uploadStream = file.OpenReadStream();
            var command =
                new UploadFileCommand(new UploadingFile(new FileInformation(file.FileName), file.ContentType,
                    uploadStream));
            var res = await sender.Send(command);
            return res.Match(_ => Results.Ok(),
                Results.BadRequest);
        })
    .DisableAntiforgery();

app.MapPost("/rename", async ([FromServices] ISender sender, [FromQuery] string fileName, [FromQuery] string newName) =>
{
    var command = new RenameFileCommand(new FileInformation(fileName), newName);
    var res = await sender.Send(command);
    return res.Match(x => Results.Ok(x.FullName), Results.BadRequest);
});

app.MapPost("/delete", async ([FromServices] ISender sender, [FromQuery] string fileName) =>
{
    var command = new DeleteFileCommand(new FileInformation(fileName));
    var res = await sender.Send(command);
    return res.Match(x => Results.Ok(x.FullName), Results.BadRequest);
});

// return file as a result of request
app.MapGet("download", async ([FromServices] ISender sender, [FromQuery] string? fileName) =>
{
    if (string.IsNullOrEmpty(fileName))
        return Results.BadRequest("No file name passed");
    var command = new DownloadFileCommand(new FileInformation(fileName));
    var fileStream = await sender.Send(command);
    return fileStream.Match(
        fileResult => Results.File(fileResult.DownloadStream, fileResult.FileInfo.ContentType),
        Results.BadRequest);
});

// downloads file into response body
app.MapGet("download-tostream/{filename}",
    async ([FromServices] ISender sender, [FromRoute] string fileName, HttpContext request) =>
    {
        var command = new DownloadFileCommand(new FileInformation(fileName), request.Response.Body);
        var fileStream = await sender.Send(command);
        return fileStream.Match(
            _ => Results.Empty,
            Results.BadRequest);
    });

app.MapPost("/download-zip",
    async ([FromKeyedServices("tempfile")] IFileDownloader downloader, [FromBody] IEnumerable<string> fileNames,
        [FromServices] ISender sender) =>
    {
        var command = new DownloadFilesArchiveCommand(fileNames.Select(x => new FileInformation(x)));
        var result = await sender.Send(command);
        return result.Match(
            file => Results.File(file.File.FullName, file.ContentType, "test.zip"),
            Results.BadRequest);
    });


app.MapGet("/stats", async ([FromServices] ISender sender, [FromQuery] string fileName) =>
{
    var command = new GetFileStatisticsCommand(new FileInformation(fileName));
    var res = await sender.Send(command);
    return res.Match(x => x is not null ? Results.Ok(x) : Results.NotFound(), Results.BadRequest);
});

app.Run();

namespace WebApiSkeleton.MinIO.Api
{
    record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
    {
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
    }

    public class DisposableFileCleanup : IDisposable
    {
        private string[] filesToDelete;
        private string zipFilename;

        public DisposableFileCleanup(string zipFilename, string[] filesToDelete)
        {
            this.filesToDelete = filesToDelete;
            this.zipFilename = zipFilename;
        }

        public void Dispose()
        {
            try
            {
                // Delete the zip file and all the files it contains
                File.Delete(zipFilename);
                foreach (var file in filesToDelete)
                {
                    File.Delete(file);
                }
            }
            catch (Exception)
            {
                // Handle exceptions if necessary
            }
        }
    }
}